<?php


?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'link-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'out_link'); ?>
		<?php echo $form->textField($model,'out_link',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'out_link'); ?>
                
	</div>
        <div class="row">
            <?php echo $form->labelEx($model,'local_link'); ?>
        <?php echo $form->textArea($model,'local_link',array('id'=>'local_link','rows'=>1, 'cols'=>80)); ?>
            </div>
	<div class="row buttons">
		<?php 
                echo CHtml::ajaxSubmitButton('Генерировать', '', array(
                'type' => 'POST', 'update' => '#local_link'));
                ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->