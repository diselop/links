<?php

/**
 * This is the model class for table "link".
 *
 * The followings are the available columns in table 'link':
 * @property integer $id
 * @property string $local_link
 * @property string $out_link
 */
class Link extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'link';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('out_link', 'required'),
                        array('out_link', 'url'),
                        array('local_link', 'safe'),
			array('local_link, out_link', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, local_link, out_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'local_link' => 'Короткая ссылка',
			'out_link' => 'Ссылка на внешний источник',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('local_link',$this->local_link,true);
		$criteria->compare('out_link',$this->out_link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Link the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        //генерация рандомной ссылкпи
        private function getShortUrl(){
            $string=Yii::app()->securityManager->generateRandomString(5);
            $normal=str_replace(array('~','_'), '0',$string );
            return strtolower($normal);
        }
        
        public function newLocalLink(){    
            $db=Yii::app()->db;
            $out_link=$this->out_link;
            $local_link=$db->createCommand("select local_link from link where out_link ='$out_link'")->queryScalar();
            // проверка на наличие ссылки в базе
            if($local_link!=null)return $local_link;
            else{   
                //генерируем ссылку пока не будет уникальной
                do {                
                    $url= $this->getShortUrl();
                    $command= $db->createCommand("select id from link where local_link ='$url'");   
                    $newUrl=$command->queryScalar();
                } while ($newUrl!=null);
                $this->local_link=$url;
                $this->out_link=$out_link;
                $this->save();                
                return $url;
            }
        }
}
